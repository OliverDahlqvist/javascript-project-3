# Assignment 3 - Pokemon Catalogue
This project was created as part of an assigment in javascript. The application allows for users to browse pokemons and "catch" them. The caught pokemons are then displayed on the profile.

## Installation

### Installing node and npm
Download and install node from https://nodejs.org/en/ and then enter the following in the terminal to install npm:
```
npm install -g npm
```
When npm is installed, install Angular by entering the following in the terminal:
```
npm install -g @angular/CLI
```
Angular is an important part of this project and is needed to run it.

### Setting up the api connection
For the application to be able to save users ("trainers" in this case) and their caught pokemons, an api is needed.
Create folder in /src/ named "environments" and create two files inside, "environment.ts" and "environmen.prod.ts".
Enter the following in the files:

environment.ts
```
export const environment = {
  production: false,
  pokemonUrl: "https://pokeapi.co/api/v2/pokemon/?limit=50&offset=",
  trainerURL: <your api url>,
  trainerAPIKey: <your api key>,
};

```
environment.prod.ts
```
export const environment = {
  production: true,
  pokemonUrl: "https://pokeapi.co/api/v2/pokemon/?limit=50&offset=",
  trainerURL: <your api url>,
};

```
Replace placeholders with your api key and api url. 

## Usage
Enter the following in the terminal to start the application:
```
ng serve
```
A local demo of the application will start at http://localhost:4200.

Enter an username to get access to the pokemon catalogue. If the username doesn't match an already existing account a new account will be created.

## Contributors
Oliver Dahlqvist @OliverDahlqvist

Adrian Mattsson @AdrianMattsson
