import { Injectable } from '@angular/core';
import { StorageKeys } from '../enums/storage-keys.enum';
import { Pokemon } from '../models/pokemon.model';
import { Trainer } from '../models/trainer.models';
import { StorageUtil } from '../utils/storage.util';

@Injectable({
  providedIn: 'root'
})
export class TrainerService {
  private _trainer?: Trainer;

  get trainer(): Trainer | undefined {
    return this._trainer;
  }

  set trainer(trainer: Trainer | undefined) {
    StorageUtil.storageSave<Trainer>(StorageKeys.Trainer, trainer!);
    this._trainer = trainer;
  }

  constructor() {
    this._trainer = StorageUtil.storageRead<Trainer>(StorageKeys.Trainer);
  }

  /**
   * Returns true or false depending on if the specified pokemon is caught.
   * @param pokemonId The pokemon to check if caught.
   * @returns True or false depending on if the pokemon is caught.
   */
  public inCaught(pokemonId: string): boolean {
    //Will return true if trainer has pokemons that are caught
    if (this.trainer) {
      return Boolean(this.trainer?.pokemon.find((pokemon: Pokemon) => pokemon.id === pokemonId));
    }
    return false;
  }

  /**
   * Adds the specified pokemon to the service's current trainer.
   * @param pokemon Pokemon to add to caught.
   */
  public addToCaught(pokemon: Pokemon): void {
    if (this._trainer) {
      this._trainer.pokemon.push(pokemon);
    }
  }

  /**
   * Removes the pokemon with given pokemonId from the service's current trainer.
   * @param pokemonId ID of the pokemon to remove from trainer's caught array.
   */
  public removeFromCaught(pokemonId: string): void {
    if (this._trainer) {
      this._trainer.pokemon = this._trainer.pokemon.filter((pokemon: Pokemon) => pokemon.id !== pokemonId);
    }
  }
}
