import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, of, switchMap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Trainer } from '../models/trainer.models';

const { trainerURL, trainerAPIKey } = environment;

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  // Dependency injection.
  constructor(private readonly http: HttpClient) { }

  public login(username: string): Observable<Trainer> {

    return this.checkUsername(username).pipe(
      switchMap((trainer: Trainer | undefined) => {
        if (trainer === undefined) {
          return this.createTrainer(username);
        }
        else {
          return of(trainer);
        }
      })
    );
  }

  /**
   * Returns the first matching trainer with given username.
   * @param username The specified username to search for.
   * @returns Trainer with specified username.
   */
  private checkUsername(username: string): Observable<Trainer | undefined> {
    return this.http.get<Trainer[]>(`${trainerURL}?username=${username}`)
      .pipe(map((response: Trainer[]) => { return response.pop() }));
  }

  /**
   * Creates a new trainer in the DB with given username.
   * @param username The name of the new trainer.
   * @returns New posted trainer.
   */
  private createTrainer(username: string): Observable<Trainer> {
    const trainer = {
      username,
      pokemon: []
    }
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      "x-api-key": trainerAPIKey
    });
    return this.http.post<Trainer>(trainerURL, trainer, { headers });
  }
}
