import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize, Observable, tap } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon } from '../models/pokemon.model';
import { Trainer } from '../models/trainer.models';
import { PokemonCatalogueService } from './pokemon-catalogue.service';
import { TrainerService } from './trainer.service';

const { trainerAPIKey, trainerURL } = environment;

@Injectable({
  providedIn: 'root'
})
export class PokemonCatchService {

  private _loading: boolean = false;

  get loading(): boolean {
    return this._loading;
  }

  constructor(
    private http: HttpClient,
    private readonly pokemonService: PokemonCatalogueService,
    private readonly trainerService: TrainerService
  ) { }


  //Add to the list of caught pokemons
  public addToCaught(id: string): Observable<Trainer> {

    //Throws error if a trainer is not found
    if (!this.trainerService.trainer) {
      throw new Error("addToCaught: No trainer found");
    }

    const trainer: Trainer = this.trainerService.trainer;
    const pokemon: Pokemon | undefined = this.pokemonService.pokemonById(id);

    //Checks if pokemon exist
    if (!pokemon) {
      throw new Error("No pokemon with id " + id);
    }

    //Checks if pokemon already is caught
    if (this.trainerService.inCaught(id)) {
      this.trainerService.removeFromCaught(id);
    } else {
      this.trainerService.addToCaught(pokemon);
    }

    const headers = new HttpHeaders({
      "content-type": "application/json",
      "x-api-key": trainerAPIKey
    })

    this._loading = true;

    //Patches with new pokemon
    return this.http.patch<Trainer>(`${trainerURL}/${trainer.id}`, {
      pokemon: [...trainer.pokemon] //Already updated
    }, {
      headers
    })
      .pipe(
        tap((updatedTrainer: Trainer) => {
          this.trainerService.trainer = updatedTrainer;
        }),
        finalize(() => {
          this._loading = false;
        })
      )
  }
}
