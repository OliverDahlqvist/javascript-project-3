import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Pokemon, PokemonData } from '../models/pokemon.model';

//gets the apiUrl from environment file
const { pokemonUrl } = environment;

let amountOfPokemonLoads = 0; //Keeps count of how many pokemons are loaded

@Injectable({
  providedIn: 'root'
})

export class PokemonCatalogueService {

  private _pokemons: Pokemon[] = [];
  private _error: string = "Error"
  private _loading: boolean = false;

  get pokemons(): Pokemon[] {
    return this._pokemons;
  }

  get error(): string {
    return this._error;
  }

  get loading(): boolean {
    return this._loading;
  }

  constructor(private readonly http: HttpClient) { }

  //This function fetches the pokemons from the url (pokemonUrl)
  public findAllPokemons(canLoad: boolean): void {
    if (canLoad === false) return; //Only loads at first initialization or when the "load more" button is clicked
    this._loading = true;
    this.http.get<PokemonData>(pokemonUrl + amountOfPokemonLoads) //pokemonUrl + amountOfPokemonLoads makes sure that the same pokemons are never loaded twice
      .pipe(
        finalize(() => {
          this._loading = false; //Stops the loading message when the pokemons are fully loaded
        })
      )
      .subscribe({
        next: (PokemonData: PokemonData) => {
          const pokemons: Pokemon[] = PokemonData.results;

          for (let i = 0; i < pokemons.length; i++) {
            let newName = pokemons[i].name;
            pokemons[i].name = newName.charAt(0).toUpperCase() + newName.slice(1); //Changes name to have an upper case first letter
            pokemons[i].img = "assets/" + `${pokemons[i].url.split("pokemon/")[1].replace("/", "")}.png`; //Creates image source from the url
            pokemons[i].id = pokemons[i].url.split("pokemon/")[1].replace("/", ""); //Creates an id from the url
          }
          this._pokemons = this._pokemons.concat(pokemons); //Adds 50 more pokemons to the list
          amountOfPokemonLoads += 50;

        }, error: (error: HttpErrorResponse) => {
          this._error = error.message;
        }
      })
  }
  public pokemonById(id: string): Pokemon | undefined {
    return this._pokemons.find((pokemon: Pokemon) => pokemon.id === id);
  }

  // public loadMorePokemons() {
  //   let newUrl = pokemonUrl.split("offset=")[1] + amountOfPokemonLoads;

  //   this.http.get<PokemonData>(newUrl)

  // }
}
