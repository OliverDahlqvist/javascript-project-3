import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { finalize } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PokemonListComponent } from '../components/pokemon-list/pokemon-list.component';
import { Ability, AbilityData, AbilityObject } from '../models/pokemon.model';
import { PokemonCatalogueService } from './pokemon-catalogue.service';

const { pokemonAbilityUrl } = environment;

@Injectable({
  providedIn: 'root'
})
export class PokemonLoadAbilitiesService {

  private _error: string = "Error"

  constructor(private readonly http: HttpClient, private readonly pokemonService: PokemonCatalogueService) { }

  /**
   * Fetches abilities from API and adds them to the component.
   * @param id ID of the pokemon to fetch abilities for.
   * @param listComponent The component to add the abilities to.
   */
  public addAbilities(id: string, listComponent: PokemonListComponent): void {
    //this._loading = true;
    this.http.get<AbilityData>(pokemonAbilityUrl + id)
      .pipe(
        finalize(() => {
          //this._loading = false;
        })
      )
      .subscribe({
        next: (AbilityData: AbilityData) => {
          const abilitiesObject: AbilityObject[] = AbilityData.abilities;
          const abilities: Ability[] = [];
          for (let i = 0; i < abilitiesObject.length; i++) {
            abilitiesObject[i].ability.name = this.formatName(abilitiesObject[i].ability.name)
            abilities.push(abilitiesObject[i].ability);
          }

          for (let i = 0; i < listComponent.pokemons.length; i++) {
            if (listComponent.pokemons[i].id == id) {
              listComponent.pokemons[i].abilities = abilities;
              break;
            }
          }
        }, error: (error: HttpErrorResponse) => {
          this._error = error.message;
        }
      })
  }
  /**
   * Splits the ability name into words and adds uppercase to first letter
   * of each word.
   * @param name Ability to format.
   * @returns Formatted ability name.
   */
  private formatName(name: string): string{
    const nameSplit: string[] = name.split("-");
    let result = "";
    for (let i = 0; i < nameSplit.length; i++) {
      result += nameSplit[i][0].toUpperCase() + nameSplit[i].slice(1) + " ";
    }
    return result;
  }
}
