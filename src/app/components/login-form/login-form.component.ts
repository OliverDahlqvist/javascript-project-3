import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Trainer } from 'src/app/models/trainer.models';
import { LoginService } from 'src/app/services/login.service';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  @Output() login: EventEmitter<void> = new EventEmitter();

  public loading: boolean = false;

  constructor(
    private readonly loginService: LoginService,
    private readonly trainerService: TrainerService) { }

  /**
   * Calls the login service to fetch or post the user with the
   * username from the login form.
   * @param loginForm The login form to use with the function.
   */
  public loginSubmit(loginForm: NgForm): void {
    const { username } = loginForm.value;
    if(loginForm.invalid){
      return;
    }
    this.loading = true;

    this.loginService.login(username).subscribe(
      {
        next: (trainer: Trainer) => {
          this.trainerService.trainer = trainer;
          this.login.emit();
          this.loading = false;
        },
        error: () => {
          this.loading = false;
        }
      });
  }
  ngOnInit(): void {
  }
}
