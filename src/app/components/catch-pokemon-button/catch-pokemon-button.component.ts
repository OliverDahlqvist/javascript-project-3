import { HttpErrorResponse } from '@angular/common/http';
import { Component, Input, OnInit } from '@angular/core';
import { Trainer } from 'src/app/models/trainer.models';
import { PokemonCatchService } from 'src/app/services/pokemon-catch.service';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-catch-pokemon-button',
  templateUrl: './catch-pokemon-button.component.html',
  styleUrls: ['./catch-pokemon-button.component.css']
})
export class CatchPokemonButtonComponent implements OnInit {

  //default state of pokemons is not caught
  public isCaught: boolean = false;

  @Input() pokemonId: string = "";

  get loading(): boolean {
    return this.pokemonCatchService.loading;
  }

  get getCaught(): boolean {
    return this.isCaught;
  }

  constructor(
    private trainerService: TrainerService,
    private readonly pokemonCatchService: PokemonCatchService
  ) { }

  ngOnInit(): void {
    //Inputs are resolved
    this.isCaught = this.trainerService.inCaught(this.pokemonId);
  }

  //On click, catches the pokemon and adds it to the trainer
  onCatchClick(): void {
    this.pokemonCatchService.addToCaught(this.pokemonId)
      .subscribe({
        next: (trainer: Trainer) => {
          this.isCaught = this.trainerService.inCaught(this.pokemonId);
        },
        //If the http request fails it sends this error message
        error: (error: HttpErrorResponse) => {
          console.log("ERROR", error.message);
        }
      })
  }
}
