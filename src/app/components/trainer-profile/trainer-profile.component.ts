import { Component, OnInit } from '@angular/core';
import { StorageKeys } from 'src/app/enums/storage-keys.enum';
import { Trainer } from 'src/app/models/trainer.models';
import { TrainerService } from 'src/app/services/trainer.service';
import { StorageUtil } from 'src/app/utils/storage.util';

@Component({
  selector: 'app-trainer-profile',
  templateUrl: './trainer-profile.component.html',
  styleUrls: ['./trainer-profile.component.css']
})
export class TrainerProfileComponent implements OnInit {

  get trainer(): Trainer {
    return this.TrainerService.trainer!
  }
  username?: string = "";

  constructor(private readonly TrainerService: TrainerService) { }

  ngOnInit(): void {
    if (this.trainer !== undefined)
      this.username = this.trainer.username;
  }

}
