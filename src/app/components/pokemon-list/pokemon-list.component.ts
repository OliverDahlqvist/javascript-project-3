import { Component, Input, OnInit, HostListener } from '@angular/core';
import { Ability, Pokemon } from 'src/app/models/pokemon.model';
import { PokemonCatalogueService } from 'src/app/services/pokemon-catalogue.service';
import { PokemonLoadAbilitiesService } from 'src/app/services/pokemon-load-abilities.service';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.css']
})
export class PokemonListComponent implements OnInit {

  @Input() pokemons: Pokemon[] = [];
  @Input() canLoadMore: boolean = false;

  //Checks if the pokemon identified by id has any abilities
  abilitiesExists(id: string): boolean {
    const pokemon = this.pokemons.find((obj) => {
      return obj.id === id;
    });
    if (pokemon !== undefined) {
      if (pokemon.abilities?.length > 0) {
        return false;
      }
    }
    return true;
  }


  constructor(private readonly pokemonLoadAbilities: PokemonLoadAbilitiesService,
    private readonly pokemonCatalogueService: PokemonCatalogueService) { }

  ngOnInit(): void {
  }

  //Loads the abilities of the pokemon when the button is clicked. This way abilities will only be loaded when needed, saving bandwidth for the user
  onLoadClick(id: string): void {
    this.pokemonLoadAbilities.addAbilities(id, this);
  }

  onLoadMoreClick(): void {
    this.pokemonCatalogueService.findAllPokemons(true);
  }
}
