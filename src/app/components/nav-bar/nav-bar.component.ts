import { Component, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { Router } from '@angular/router';
import { StorageKeys } from 'src/app/enums/storage-keys.enum';
import { TrainerService } from 'src/app/services/trainer.service';
import { StorageUtil } from 'src/app/utils/storage.util';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  constructor(private readonly trainerService: TrainerService,
    private readonly router: Router) { }

  //If a trainer exists, returns true
  get trainerExists(): boolean {
    if (this.trainerService.trainer === undefined)
      return true;
    else
      return false;
  }
  ngOnInit(): void {
  }

  //Clears the trainer from the local storage
  storageClear(): void {
    this.trainerService.trainer = undefined;
    this.router.navigateByUrl("/login");
  }

}
