
export interface Pokemon {
    name: string,
    url: string, //This is the url imported from the api
    img: string, //This is the image src, created from the url when fetched
    id: string, //This is the id of the pokemon, created from the url when fetched
    abilities: Ability[];
}

export interface AbilityObject {
    ability: Ability;
}

export interface Ability {
    name: string;
}

export interface AbilityData {
    abilities: Array<AbilityObject>;
}

export interface PokemonData {
    results: Array<Pokemon>;
}

