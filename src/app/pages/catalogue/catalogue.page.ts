import { Component, OnInit } from '@angular/core';
import { PokemonCatalogueService } from 'src/app/services/pokemon-catalogue.service';
import { Pokemon } from 'src/app/models/pokemon.model';

let firstTimeLoading = true;

@Component({
  selector: 'app-catalogue',
  templateUrl: './catalogue.page.html',
  styleUrls: ['./catalogue.page.css']
})

export class CataloguePage implements OnInit {

  get pokemons(): Pokemon[] {
    return this.PokemonCatalogueService.pokemons;
  }

  get loading(): boolean {
    return this.PokemonCatalogueService.loading;
  }

  get error(): string {
    return this.PokemonCatalogueService.error;
  }

  constructor(
    private readonly PokemonCatalogueService: PokemonCatalogueService
  ) { }

  ngOnInit(): void {
    this.PokemonCatalogueService.findAllPokemons(firstTimeLoading);
    firstTimeLoading = false;
  }

}
