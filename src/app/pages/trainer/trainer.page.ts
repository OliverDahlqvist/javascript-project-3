import { Component, OnInit } from '@angular/core';
import { Trainer } from 'src/app/models/trainer.models';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.page.html',
  styleUrls: ['./trainer.page.css']
})
export class TrainerPage implements OnInit {

  get trainer(): Trainer {
    return this.trainerService.trainer!;
  }

  constructor(
    private readonly trainerService: TrainerService
  ) { }

  ngOnInit(): void {
    this.trainerService.trainer;
  }

}
