export class StorageUtil {

    /**
     * Saves to the given storage key with the given value.
     * @param key Key name to save to.
     * @param value Value to save with.
     */
    public static storageSave<T>(key: string, value: T): void {
        if (value === undefined) {
            this.storageClear(key);
        }
        else {
            sessionStorage.setItem(key, JSON.stringify(value));
        }
    }

    /**
     * Reads the value at the given key name and returns it.
     * @param key Key name to read from.
     * @returns Value from key with specified type.
     */
    public static storageRead<T>(key: string): T | undefined {
        const storedValue = sessionStorage.getItem(key);
        try {
            if (storedValue) {
                return JSON.parse(storedValue);
            }
            else {
                return undefined;
            }
        }
        catch (e) {
            sessionStorage.removeItem(key);
            return undefined;
        }
    }
    /**
     * Clears the given key's value.
     * @param key Key name to clear.
     */
    public static storageClear(key: string) {
        sessionStorage.removeItem(key);
    }
}