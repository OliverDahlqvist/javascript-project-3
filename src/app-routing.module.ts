import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { CataloguePage } from "./app/pages/catalogue/catalogue.page";
import { TrainerPage } from "./app/pages/trainer/trainer.page";
import { LoginPage } from "./app/pages/login/login.page";
import { AuthGuard } from "./app/guards/auth.guard";
import { LoginGuard } from "./app/guards/login.guard";

const routes: Routes = [
    {
        path: "",
        pathMatch: "full",
        redirectTo: "/login"
    },
    {
        path: "login",
        component: LoginPage,
        canActivate: [LoginGuard]
    },
    {
        path: "trainer",
        component: TrainerPage,
        canActivate: [AuthGuard]
    },
    {
        path: "catalogue",
        component: CataloguePage,
        canActivate: [AuthGuard]
    }
]

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})
export class AppRoutingModule {

}